package dtclient

import (
	"bytes"
	"github.com/pkg/errors"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)

type RestApiClient struct {
	BaseUri string
	Tenant  string
	Token   string
}

func (dc *RestApiClient) CreateUrl(endpoint string) string {
	if strings.HasPrefix(dc.Tenant, "/e/") {
		return dc.BaseUri + dc.Tenant + endpoint
	}
	return dc.BaseUri + "/e/" + dc.Tenant + endpoint
}

func (dc *RestApiClient) DoPut(endpoint string, body []byte) (string, error) {
	return dc.doRequest(endpoint, bytes.NewReader(body), "PUT")
}

func (dc *RestApiClient) DoPost(endpoint string, body []byte) (string, error) {
	return dc.doRequest(endpoint, bytes.NewReader(body), "POST")
}

func (dc *RestApiClient) DoGet(endpoint string) (string, error) {
	return dc.doRequest(endpoint, nil, "GET")
}

func (dc *RestApiClient) doRequest(endpoint string, body io.Reader, method string) (string, error) {
	req, err := http.NewRequest(method, dc.CreateUrl(endpoint), body)
	if err != nil {
		return "", err
	}
	req.Header.Add("Authorization", "Api-Token "+dc.Token)
	if method == "POST" || method == "PUT" {
		req.Header.Add("Content-Type", "application/json")
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	bs, err := ioutil.ReadAll(resp.Body)
	str := string(bs)
	if resp.StatusCode == 429 {
		resettime, _ := strconv.ParseInt(resp.Header.Get("X-RateLimit-Reset"), 10, 64) // UTC millis
		time.Sleep(time.Until(time.Unix(0, resettime*1e6)))
		return dc.doRequest(endpoint, body, method)
	}
	if resp.StatusCode > 299 {
		return "", errors.New("Statuscode " + resp.Status + " returned. Body: " + str)
	}
	return str, nil
}
