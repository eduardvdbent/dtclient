package dtclient

import (
	"os"
	"testing"
)

func TestDebugUIClient_initDynatrace(t *testing.T) {
	dblogin := DebugUIClient{
		Baseuri: os.Getenv("cluster"),
		Token:   os.Getenv("debugtoken"),
		Version: "csrf",
	}
	err := dblogin.InitDynatrace()
	if err != nil {
		t.Error(err)
	}
}
