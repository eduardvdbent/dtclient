package dtclient

import (
	"encoding/json"
	"os"
	"strconv"
	"testing"
)

func TestRestApiClient_DoGet(t *testing.T) {
	type fields struct {
		BaseUri string
		Tenant  string
		Token   string
	}
	type args struct {
		endpoint string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		{"Simple test", fields{
			BaseUri: os.Getenv("cluster"),
			Tenant:  os.Getenv("environment"),
			Token:   "",
		}, args{"/api/v1/time"}, 1569875828437, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dc := &RestApiClient{
				BaseUri: tt.fields.BaseUri,
				Tenant:  tt.fields.Tenant,
				Token:   tt.fields.Token,
			}
			got, err := dc.DoGet(tt.args.endpoint)
			if (err != nil) != tt.wantErr {
				t.Errorf("DoGet() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			goti, err := strconv.Atoi(got)
			if err != nil || goti < tt.want {
				t.Errorf("DoGet() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRestApiClient_DoGet_Dashboards(t *testing.T) {
	type fields struct {
		BaseUri string
		Tenant  string
		Token   string
	}
	type args struct {
		endpoint string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		{"Simple test", fields{
			BaseUri: os.Getenv("cluster"),
			Tenant:  os.Getenv("environment"),
			Token:   os.Getenv("token"),
		}, args{"/api/config/v1/dashboards"}, 1569875828437, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dc := &RestApiClient{
				BaseUri: tt.fields.BaseUri,
				Tenant:  tt.fields.Tenant,
				Token:   tt.fields.Token,
			}
			got, err := dc.DoGet(tt.args.endpoint)
			if (err != nil) != tt.wantErr {
				t.Errorf("DoGet() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			var gotResp SimpleResponse
			err = json.Unmarshal([]byte(got), &gotResp)
			if (err != nil) != tt.wantErr {
				t.Errorf("DoGet() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if len(gotResp.Values) == 0 && len(gotResp.Dashboards) == 0 {
				t.Errorf("No correct payload received: " + got)
			}
		})
	}
}

type SimpleResponse struct {
	Dashboards []map[string]string `json:"dashboards"`
	Values     []map[string]string `json:"values"`
}
