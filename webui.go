package dtclient

import (
	"github.com/pkg/errors"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"sync"
	"time"
)

type WebUIClient struct {
	Cluster     string
	Environment string
	Useragent   string
	Username    string
	Password    string
	Viewer      bool
	auth        *http.Cookie
	csrftoken   string
	updateTime  time.Time
	updateLock  sync.RWMutex
}

func (wuc *WebUIClient) Login() error {
	wuc.updateLock.Lock()
	defer func() {
		wuc.updateTime = time.Now()
		wuc.updateLock.Unlock()
	}()
	body, err := wuc.loadLoginPage()
	if err != nil {
		return err
	}
	csrf := getToken(body)
	err = wuc.loginAndGetCsrf(csrf)
	return err
}

func (wuc *WebUIClient) updateCredentials() {
	if time.Now().Sub(wuc.updateTime) > 1*time.Minute {
		wuc.Login()
	}
}

func (wuc *WebUIClient) loadLoginPage() (string, error) {
	resp, err, _ := wuc.doRequest("GET",wuc.Cluster, nil)
	if err != nil {
		return "", err
	}
	return string(resp), nil
}

func getToken(html string) string {
	var re = regexp.MustCompile(`(?m)(?s)csrf_token".*?value="(.*?)"/>`)
	matches := re.FindAllStringSubmatch(html, -1)
	if matches == nil {
		return ""
	}
	return matches[0][1]
}

func getTokenEnv(html string) string {
	var re = regexp.MustCompile(`var csrf_token = '(.*?)'`)
	matches := re.FindAllStringSubmatch(html, -1)
	if matches == nil {
		return ""
	}
	return matches[0][1]
}

func (wuc *WebUIClient) loginAndGetCsrf(csrf string) error {
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	form := url.Values{}
	form.Add("user", wuc.Username)
	form.Add("pass", wuc.Password)
	form.Add("X-CSRFToken", csrf)
	req, err := http.NewRequest("POST", wuc.Cluster+"/login", strings.NewReader(form.Encode()))
	if err != nil {
		return err
	}

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("User-Agent", wuc.Useragent)

	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	for _, v := range resp.Cookies() {
		if v.Name == "apmsessionid" {
			wuc.auth = v
		}
	}
	if wuc.auth == nil {
		return errors.New("Cookie apmsessionid was not found.")
	}

	if wuc.Viewer {
		wuc.csrftoken = getTokenEnv(string(body))
		return nil
	}

	resp2, err, _ := wuc.doRequest("GET",wuc.CreateUrl(""), nil)
	if err != nil {
		return err
	}
	wuc.csrftoken = getTokenEnv(string(resp2))
	if wuc.csrftoken == "" {
		return errors.New("Login failed.")
	}
	return nil
}

func (wuc *WebUIClient) CreateUrl(uri string) string {
	res := wuc.Cluster
	if !strings.HasPrefix(wuc.Environment, "/e/") {
		res += "/e/"
	}
	res += wuc.Environment
	if !strings.HasSuffix(wuc.Environment, "/") {
		res += "/"
	}
	return res + uri
}

func (wuc *WebUIClient) DoGet(uri string) ([]byte, error) {
	wuc.updateLock.RLock()
	resp, err, _ := wuc.doRequest("GET",wuc.CreateUrl(uri), nil)
	wuc.updateLock.RUnlock()
	return resp, err
}

func (wuc *WebUIClient) DoPost(uri string, body string) ([]byte, error) {
	wuc.updateLock.RLock()
	resp, err, _ := wuc.doRequest("POST",wuc.CreateUrl(uri), strings.NewReader(body))
	wuc.updateLock.RUnlock()
	return resp, err
}

func (wuc *WebUIClient) DoGetJson(uri string) ([]byte, error) {
	wuc.updateLock.RLock()
	resp, err, contenttype := wuc.doRequest("GET",wuc.CreateUrl(uri), nil)
	wuc.updateLock.RUnlock()
	if !strings.Contains(contenttype, "json") {
		go wuc.updateCredentials()
		return nil, errors.New("Credentials invalid")
	}
	return resp, err
}

func (wuc *WebUIClient) doRequest(method string, uri string, body io.Reader) ([]byte, error, string) {
	req, err := http.NewRequest(method, uri, body)
	if err != nil {
		return nil, err, ""
	}

	if method == "POST" || method == "PUT" {
		req.Header.Add("Content-Type", "application/json")
	}
	req.Header.Set("User-Agent", wuc.Useragent)
	if wuc.csrftoken != "" {
		req.Header.Set("X-CSRFToken", wuc.csrftoken)
	}
	if wuc.auth != nil {
		req.AddCookie(wuc.auth)
	}
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err, ""
	}
	defer res.Body.Close()

	rbod, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err, ""
	}
	if res.StatusCode == 401 {
		go wuc.updateCredentials()
		return nil, errors.New("Credentials invalid: " + string(rbod)), ""
	}
	return rbod, nil, res.Header.Get("Content-Type")
}
