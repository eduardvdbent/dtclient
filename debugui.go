package dtclient

import (
	"bytes"
	"crypto/tls"
	"github.com/pkg/errors"
	"io"
	"io/ioutil"
	log "github.com/sirupsen/logrus"
	"net/http"
	"net/http/httputil"
	"strconv"
	"strings"
	"sync"
	"time"
)

type DebugUIClient struct {
	Baseuri    string
	Token      string
	Version    string
	MC         []http.Cookie
	Auth       *http.Cookie
	Csrf       string
	updateTime time.Time
	updateLock sync.RWMutex
}

func (dbc *DebugUIClient) InitDynatrace() error {
	dbc.updateLock.Lock()
	defer func() {
		dbc.updateTime = time.Now()
		dbc.updateLock.Unlock()
	}()
	var err error
	dbc.Auth, err = dbc.login()
	if err != nil {
		return err
	}
	if dbc.Version == "csrf" {
		dbc.Csrf, err = dbc.getCsrfToken()
		if err != nil {
			return err
		}
	}
	return nil
}

func (dbc *DebugUIClient) updateCredentials() {
	if time.Now().Sub(dbc.updateTime) > 1*time.Minute {
		dbc.InitDynatrace()
	}
}

var tr = &http.Transport{
	TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
}

func (dbc *DebugUIClient) DoGet(uri string) ([]byte, error) {
	dbc.updateLock.RLock()
	defer dbc.updateLock.RUnlock()

	return dbc.doReq(uri, "GET", nil)
}

func (dbc *DebugUIClient) DoPost(uri string, body []byte) ([]byte, error) {
	dbc.updateLock.RLock()
	defer dbc.updateLock.RUnlock()

	return dbc.doReq(uri, "POST", bytes.NewReader(body))
}

func (dbc *DebugUIClient) DoPut(uri string, body []byte) ([]byte, error) {
	dbc.updateLock.RLock()
	defer dbc.updateLock.RUnlock()

	return dbc.doReq(uri, "PUT", bytes.NewReader(body))
}

func (dbc *DebugUIClient) doReq(uri string, method string, reqbody io.Reader) ([]byte, error) {
	client := http.Client{Transport: tr}

	queryReq, err := http.NewRequest(method, dbc.Baseuri+uri, reqbody)
	if err != nil {
		return nil, err
	}
	if dbc.Auth != nil {
		queryReq.AddCookie(dbc.Auth)
	}
	if dbc.Csrf != "" && method == "GET" {
		queryReq.Header.Add("OWASP-CSRFTOKEN", dbc.Csrf)
	}

	resp, err := client.Do(queryReq)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		output, _ := httputil.DumpRequest(queryReq, false)
		log.Error(string(output))
		output, _ = httputil.DumpResponse(resp, false)
		log.Error(string(output))
		return nil, errors.New("Error, statuscode returned: " + strconv.Itoa(resp.StatusCode) + " for url " + uri)
	}
	body, err := ioutil.ReadAll(resp.Body)
	return body, nil
}

func (dbc *DebugUIClient) login() (*http.Cookie, error) {
	client := http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
		Transport: tr,
	}

	req, err := http.NewRequest("GET", dbc.Baseuri+"debugui/index.jsp?loginWithDevOpsToken="+dbc.Token, nil)
	if err != nil {
		return nil, err
	}

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode > 399 {
		log.Error("Login failed.")
		output, _ := httputil.DumpRequest(req, false)
		log.Error(string(output))
		output, _ = httputil.DumpResponse(resp, false)
		log.Error(string(output))
	}

	if resp.StatusCode == 401 || resp.StatusCode == 499 || resp.StatusCode == 500 {
		go dbc.updateCredentials()
		return nil, errors.New("Credentials invalid. Server restarted or session timed out.")
	}

	for _, v := range resp.Cookies() {
		if v.Name == "apmsessionid" {
			return v, nil
		}
	}

	output, _ := httputil.DumpRequest(req, false)
	log.Error(string(output))
	output, _ = httputil.DumpResponse(resp, false)
	log.Error(string(output))

	return nil, errors.New("Login failed, cookie not found")
}

func (dbc *DebugUIClient) getCsrfToken() (string, error) {
	var csrfb string
	_, err := dbc.doReq("debugui/index.jsp", "GET", nil)
	csrfBody, err := dbc.doReq("debugui/debug/OwaspCsrfGuard", "GET", nil)

	if err != nil {
		return "", err
	}

	csrfStrings := strings.Split(string(csrfBody),"\n")
	for _, v := range csrfStrings {
		if strings.Contains(v, "var masterTokenValue = ") {
			if strings.Contains(v,"= '") {
				token1 := strings.Split(v,  "= '")[1]
				csrfb = strings.Split(token1,"'")[0]
			}
		}
	}

	if csrfb == "" {
		return "", errors.New("CSRF Token has wrong format -  does not contain ':' :" + csrfb)
	}

	resp, err := dbc.doReq("debugui/debug/servers?clusterUUID=true", "GET", nil)

	log.Printf("Successfully authenticated with cluster %s",resp)

	if err != nil {
		return "", err
	}

	return csrfb, nil
}
