package dtclient

import (
	"os"
	"testing"
)

func TestWebUIClient_Login(t *testing.T) {
	wuc := &WebUIClient{
		Cluster:     os.Getenv("cluster"),
		Environment: os.Getenv("environment"),
		Useragent:   "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15",
		Username:    os.Getenv("username"),
		Password:    os.Getenv("password"),
		Viewer:      false,
	}
	err := wuc.Login()
	if err != nil {
		t.Error(err)
	}
	if wuc.auth == nil {
		t.Error("Not authenticated during test.")
		t.Error(err)
	}
}

func TestWebUIClient_Login_Wrongpassword(t *testing.T) {
	wuc := &WebUIClient{
		Cluster:     os.Getenv("cluster"),
		Environment: os.Getenv("environment"),
		Useragent:   "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15",
		Username:    os.Getenv("username"),
		Password:    "so wrong!",
		Viewer:      false,
	}
	err := wuc.Login()
	if err == nil {
		t.Error("Should not have succeeded.")
	}
}
